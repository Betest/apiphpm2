<?php
require_once('utilidades.php');

if(isset($_GET['url'])){
    $var = $_GET['url'];
   

    if ($_SERVER['REQUEST_METHOD']=='GET'){
        
        //capturar un dato, validar que sea numero y asignarlo a la variable (el ultimo parametro es la cantidad de caracteres)
        $numero = intval(preg_replace('/[^0-9]+/','',$var),10);

        print_r($numero);

        switch($var){
            case "appointments";
                    $appointments = getAppointments();
                    print_r(json_encode($appointments));
                    http_response_code(200);
            break;

            case "appointments/$numero";
                    $appointment = getAppointmentById($numero);
                    print_r(json_encode($appointment));
                    http_response_code(200);
            break;

            default;
        }

    }else if ($_SERVER['REQUEST_METHOD']=='POST'){
        $postBody = file_get_contents("php://input");
        $appointment = json_decode($postBody,true);

        if(json_last_error()==0){
            switch($var){
                case "appointment";
                        saveAppointment($appointment);                        
                        http_response_code(200);
                break;
    
                default;
            }
        }else{
            http_response_code(400);
        }

    }else if ($_SERVER['REQUEST_METHOD']=='PUT'){
        $postBody = file_get_contents("php://input");
        $appointment = json_decode($postBody,true);

        if(json_last_error()==0){
            switch($var){
                case "appointment";
                        updateAppointment($appointment);                        
                        http_response_code(200);
                break;

                case "appointment/delete/$numero";
                        deleteAppointment($appointment);                        
                        http_response_code(200);
                break;
    
                default;
            }
        }else{
            http_response_code(400);
        }

    } else {
        http_response_code(405);
    }

}else{?>
    <!--metadata -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="container">
    <h1>Metadata</h1>
    <div>
        <p>Appointments</p>
        <code>
            GET /appointments
            <br>
            GET /appointment/$id
        </code>
        <br>
        <code>
            POST /appointment
        </code>
        <br>
        <code>
            PUT /appointment
        </code>
        <br>
        <code>
            PUT /appointment/delete/$id
        </code>
        
    </div>
</div>

<?php



}





?>