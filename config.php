<?php



$server = "localhost";
$user = "root";
$pwd = "";
$database = "apiphp";
$port = "3306";



//string de conexion

$conexion = new mysqli($server, $user, $pwd, $database, $port);

if($conexion -> connect_errno){
    die($conexion -> connect_error);
}


// guardar, modificar, eliminar
function NonQuery($sqlstr, $conexion = null){
    if(!$conexion)global $conexion;
    $result = $conexion->query($sqlstr);
    return $conexion -> affected_rows;
}



//select (get)
function listTasks($sqlstr, &$conexion = null){
    if(!$conexion)global $conexion;
    $result = $conexion->query($sqlstr);
    $resultArray = array();
    foreach( $result as $registro){
        $resultArray[] = $registro;
    }
    return $resultArray;
}

//utf-8

function convutf8($array){
    array_walk_recursive($array,function(&$item,$key){
        if(!mb_detect_encoding($item,'utf-8',true)){
            $item = utf8_encode($item);            
        }
    });
    return $array;
}

?>